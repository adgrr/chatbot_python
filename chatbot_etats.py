#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, logging, requests, time, math, subprocess

from telegram import (ReplyKeyboardMarkup, ReplyKeyboardRemove)
from telegram.ext import (Updater, CommandHandler, MessageHandler, Filters, RegexHandler,
                          ConversationHandler)

bot_token = sys.argv[1] #Le premier paramètre (0 étant le nom du fichier)

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)

logger = logging.getLogger(__name__)

START, TYPE_NOURRITURE, RESTO_RESULTATS, RESTO_DETAILS, MAPS, TOP, END_FLOW, SORTIE_RECO, TRANSPORT = range(9)


def start(bot, update):
    reply_keyboard = [['Restaurant', 'Sortie']]

    update.message.reply_text(
        'Salut ! Je suis le GenevaBot!\n'
        'Je peux t\'aider à trouver un un restaurant ou une sortie !\n\n'
        'Pour stopper la conversation envoie /cancel .\n'
        'Ou alors utilise /retour pour revenir en arrière\n\n'
        'Du coup... ? Restaurant ou sortie ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return START


def location(bot, update):
    update.message.reply_text('Excellent choix  \U0001f35d:\n')
    update.message.reply_location(46.190950699999995, 6.1301723)
    update.message.reply_text('A la prochaine!')

    return SORTIE_RECO


def skip_location(bot, update):
    user = update.message.from_user
    logger.info("User %s did not send a location.", user.first_name)
    update.message.reply_text('You seem a bit paranoid! '
                              'At last, tell me something about yourself.')

    return BIO


def cancel(bot, update):
    user = update.message.from_user
    logger.info("Le user %s a stoppé la conversation.", user.first_name)
    update.message.reply_text('Alors comme ça on part sans dire au revoir... Je retiens',
                              reply_markup=ReplyKeyboardRemove())

    return ConversationHandler.END


def error(bot, update, error):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, error)


def resto_choix(bot,update):
    reply_keyboard = [['Chinois', 'Français', 'Mexicain', 'Italien', 'Thai']]

    update.message.reply_text(
        'Pour stopper la conversation envoie /cancel .\n'
        'Restaurant ou sortie ?',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return TYPE_NOURRITURE


def affiche_resto(bot, update):
    reply_keyboard = [['Mandarin', 'Café du Levant', 'Pasta Doro', 'Thai Phuket', 'Fajitas Express']]

    update.message.reply_text(
        'Pour stopper la conversation envoie /cancel .\n'
        'Voici le top 5 des restaurants, sélectionnez-en un !',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return MAPS


def sortie_choix(bot,update):
    reply_keyboard = [['Musée', 'Bars', 'Clubs']]
    update.message.reply_text(
        'Pour stopper la conversation envoie /cancel .\n'
        'Choisissez le type de sortie: \n',
        reply_markup=ReplyKeyboardMarkup(reply_keyboard, one_time_keyboard=True))

    return TOP


def afficher_top_musee(bot,update):
    update.message.reply_text(
        'Pour stopper la conversation envoie /cancel .\n'
        'Voici le top 3 des bars:\n'
        '- Museum\n'
        '- Histoire du monde\n'
        '- The Museum of Geneva\n',
        reply_markup=ReplyKeyboardRemove())

    return SORTIE_RECO


def afficher_top_bars(bot, update):
    update.message.reply_text(
        'Pour stopper la conversation envoie /cancel .\n'
        'Voici le top 3 des bars:\n'
        '- Coyote Bar\n'
        '- Meltdown Bar\n'
        '- Café TakeItEasy\n',
        reply_markup=ReplyKeyboardRemove())

    return SORTIE_RECO


def afficher_top_club(bot, update):
    update.message.reply_text(
        'Pour stopper la conversation envoie /cancel .\n'
        'Voici le top 3 des bars:\n'
        '- Le Café Club\n'
        '- The Bar\n'
        '- Geneva Takedown',
        reply_markup=ReplyKeyboardRemove())
    return SORTIE_RECO


# TRANSPORT
def appeler_opendata(path):
    url = "http://transport.opendata.ch/v1/" + path
    reponse = requests.get(url)
    return reponse.json()


def calcul_temps_depart(timestamp):
    seconds = timestamp-time.time()
    minutes = math.floor(seconds/60)
    if minutes < 1:
        return "FAUT COURIR!"
    if minutes > 60:
        return "> {} h.".format(math.floor(minutes/60))
    return "dans {} min.".format(minutes)


# Preparation des messages

def afficher_arrets(update, arrets):
    texte_de_reponse = "Voici les arrets:\n"
    for station in arrets['stations']:
        if station['id'] is not None:
            texte_de_reponse += "\n/a" + station['id'] + " " + station['name']
    update.message.reply_text(texte_de_reponse)


def afficher_departs(update, departs):
    texte_de_reponse = "Voici les prochains departs:\n\n"
    for depart in departs['stationboard']:
        texte_de_reponse += "{} {} dest. {} - {}\n".format(
            depart['category'],
            depart['number'],
            depart['to'],
            calcul_temps_depart(depart['stop']['departureTimestamp'])
        )
    texte_de_reponse += "\nAfficher a nouveau: /a" + departs['station']['id']

    coordinate = departs['station']['coordinate']
    update.message.reply_location(coordinate['x'], coordinate['y'])
    update.message.reply_text(texte_de_reponse)



# Les differentes reponses

def bienvenue(bot, update):
    update.message.reply_text("Merci d'envoyer votre localisation (via piece jointe ou simplement en texte)")

    return TRANSPORT

def lieu_a_chercher(bot, update):
    resultats_opendata = appeler_opendata("locations?query=" + update.message.text)
    afficher_arrets(update, resultats_opendata)


def coordonnees_a_traiter(bot, update):
    location = update.message.location
    resultats_opendata = appeler_opendata("locations?x={}&y={}".format(location.latitude, location.longitude))
    afficher_arrets(update, resultats_opendata)


def details_arret(bot, update):
    arret_id = update.message.text[2:]
    afficher_departs(update, appeler_opendata("stationboard?id=" + arret_id))

def main():
    # Create the EventHandler and pass it your bot's token.
    updater = Updater(bot_token)

    # Get the dispatcher to register handlers
    dp = updater.dispatcher

    # Les différents états
    conv_handler = ConversationHandler(
        # Commande qui démarre le flux /start ou /transport
        entry_points=[CommandHandler('start', start), CommandHandler('transport', bienvenue)],

        states={
            # Restaurant ou Sortie
            START: [
                RegexHandler('(Restaurant)$', resto_choix),
                RegexHandler('(Sortie)$', sortie_choix)
            ],

            # Si "Restaurant a été choisi. Choix du type de restaurant
            TYPE_NOURRITURE: [
                RegexHandler('(Chinois)$', affiche_resto),
                RegexHandler('(Français)$', affiche_resto),
                RegexHandler('(Mexicain)$', affiche_resto),
                RegexHandler('(Italien)$', affiche_resto),
                RegexHandler('(Thai)$', affiche_resto),
                CommandHandler('retour', start)
            ],

            # Peu importe le type de restaurant choisi, une liste fixe de choix de restaurant
            MAPS: [
                RegexHandler('(Mandarin)$', location),
                RegexHandler('(Café du Levant)$', location),
                RegexHandler('(Pasta Doro)$', location),
                RegexHandler('(Thai Phuket)$', location),
                RegexHandler('(Fajitas Express)$', location),
                CommandHandler('retour', affiche_resto)
            ],

            # Au start, si l'utilisateur choisi la "Sortie"
            TOP: [
                RegexHandler('(Musée)$', afficher_top_musee),
                RegexHandler('(Bars)$', afficher_top_bars),
                RegexHandler('(Clubs)$', afficher_top_club),
                CommandHandler('retour', start)
            ],

            SORTIE_RECO: [
                CommandHandler('retour', sortie_choix)
            ],

            TRANSPORT: [
                CommandHandler('start', start),
                MessageHandler(Filters.text, lieu_a_chercher),
                MessageHandler(Filters.location, coordonnees_a_traiter),
                CommandHandler('cancel', cancel),
                MessageHandler(Filters.command, details_arret)
            ],

        },

        fallbacks=[CommandHandler('cancel', cancel)]
    )

    dp.add_handler(conv_handler)

    # log all errors
    dp.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Run the bot until you press Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()

if __name__ == '__main__':
     main()